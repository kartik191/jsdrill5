function limitFunctionCallCount(cb, n){
    let count = n;
    if(typeof(n) !== 'number'){
        count = 0;
    }
    function callCb(){
        if(count >= 1){
            count--;
            return cb();
        }
        else{
            return null;
        }
    }
    return callCb;
}

module.exports = limitFunctionCallCount;