function cacheFunction(cb){
    let cache = {};
    function callCb(...args){
        let key = args.join(' ');
        if(cache[key] === undefined){
            let result = cb(...args);
            cache[key] = result;    
            return result;
        }
        else{
            return cache[key];
        }
    }
    return callCb;
}

module.exports = cacheFunction;