const limitFunctionCallCount = require('../limitFunctionCallCount');

let result = limitFunctionCallCount(()=>console.log('hello world number'), 4);
result();
result();
result();
result();
result();

result = limitFunctionCallCount(()=>console.log('hello world undefined'),undefined);
result();

result = limitFunctionCallCount(()=>console.log('hello world null'),null);
result();

result = limitFunctionCallCount(()=>console.log('hello world NaN'),NaN);
result();

result = limitFunctionCallCount(()=>console.log('hello world "12"'),'12');
result();

result = limitFunctionCallCount(()=>console.log('hello world'),[1,2,3]);
result();