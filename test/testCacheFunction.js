const cacheFunction = require('../cacheFunction');

let result = cacheFunction(arg=>arg+arg);
console.log(result(5));
console.log(result(5));
console.log(result(15));
console.log(result(5));
console.log(result(15));
console.log(result(5));
console.log(result('15'));
console.log(result('5'));
console.log(result('str'));
console.log(result('str'));
console.log(result(null));  
console.log(result(undefined)); 
console.log(result()); 

result = cacheFunction(arg=>Math.random());
console.log(result(5));
console.log(result(15));
console.log(result(5));
console.log(result(15));
console.log(result('15'));

result = cacheFunction((a,b,c)=>a+b+c);
console.log(result(5,10,15));
console.log(result(15,20,30));
console.log(result(5,10,15));
console.log(result(15,20,30));
console.log(result(15,20,'30'));

result = cacheFunction((a,b)=>a*b);
console.log(result(5,10));