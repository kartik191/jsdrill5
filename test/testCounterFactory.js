const counterFactory = require('../counterFactory');

let result = counterFactory();
console.log(result.increment());
console.log(result.increment());
console.log(result.decrement());
console.log(result.increment());
console.log(result.decrement());
console.log(result.decrement());
console.log(result.decrement());

result = counterFactory();
console.log(result.increment());
console.log(result.increment());
console.log(result.increment());
console.log(result.decrement());
console.log(result.decrement());

result = counterFactory();
console.log(result.decrement());
console.log(result.decrement());